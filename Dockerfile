FROM python:3-alpine
LABEL author="Los manines"
RUN apk update
RUN apk add --no-cache openssh
RUN pip3 install flask gunicorn wakeonlan
COPY main.py /opt/
WORKDIR /opt
EXPOSE 5000

WORKDIR /opt



CMD ["gunicorn", "--bind", "0.0.0.0:5000", "main:app", "-w", "2", "--threads", "128"]
