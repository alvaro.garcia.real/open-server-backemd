docker buildx build --platform linux/arm/v7 -t startpc-backend:"$1" --load .
docker image tag startpc-backend:"$1" alvarogarcia/startpc-backend:"$1"
docker image push alvarogarcia/startpc-backend:"$1"
