import json
import os
import subprocess

from flask import Flask
from wakeonlan import send_magic_packet

app = Flask(__name__)

valheim_mac = os.environ["VALHEIM_MAC"]
valheim_ip = os.environ["VALHEIM_IP"]

masterrace_mac = os.environ["MASTERRACE_MAC"]
masterrace_ip = os.environ["MASTERRACE_IP"]

@app.route('/<service>/<action>', methods=['GET'])
def interaction(service, action):
    if service == "valheim":
        if action == "on":
            return start(valheim_mac)
        if action == "off":
            return stop(valheim_ip)
    if service == "masterrace":
        if action == "on":
            return start(masterrace_mac)
        if action == "off":
            return stop(masterrace_ip)

    return json.dumps({"status": False})


def start(mac):
    send_magic_packet(mac)
    return json.dumps({"status": True})


def stop(ip):
    subprocess.run(["ssh", "-t", ("agr@%s" % ip), "sudo", "poweroff"])
    return json.dumps({"status": True})


@app.route('/<service>/status', methods=['GET'])
def status(service):
    if service == "valheim":
        response = os.system("ping -c 1 -W 1 " + valheim_ip)
        if response == 0:
            return json.dumps({"status": True})
    if service == "masterrace":
        response = os.system("ping -c 1 -W 1 " + masterrace_ip)
        if response == 0:
            return json.dumps({"status": True})
    return json.dumps({"status": False})


if __name__ == "__main__":
    app.run(threaded=True, host= '0.0.0.0')

